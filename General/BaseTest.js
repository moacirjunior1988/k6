
let resultado = "";

export function generatorStringRandomUppercase(size) {
    var stringRandom= '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var i = 0; i <= size; i++) {
        stringRandom += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return stringRandom;
}

export function generatorStringRandomLowercase(size) {
    var stringRandom= '';
    var characters = 'abcdefghijklmnopqrstuvwxyz';
    for (var i = 0; i <= size; i++) {
        stringRandom += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return stringRandom;
}

export function generatorIntegerRandom(size){
    var integerRandom ='';
    var characters = '1234567890';
    for (var i = 0; i <= size; i++) {
        integerRandom += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return integerRandom;
}

export function converterStringToBase64(value) 
{    
    var stringBase64 = Base64.encode(value);
    return stringBase64;
}

export function generatorNumberControlClient(sizeLetters, sizeNumbers)
{
    var numberControlClient = generatorStringRandomUppercase(sizeLetters) + generatorIntegerRandom(sizeNumbers);
    return numberControlClient;
}

export function gerarCpf() {
    var num1 = aleatorio().toString();
    var num2 = aleatorio().toString();
    var num3 = aleatorio().toString();
    
    var dig1 = digPri(num1,num2,num3);
    var dig2 = digSeg(num1,num2,num3,dig1);
    var cpf = num1+"."+num2+"."+num3+"-"+dig1+""+dig2;

    return cpf;
}

export function gerarCnpj(comPontos){
    var n = 9;
    var n1 = gera_random(n);
    var n2 = gera_random(n);
    var n3 = gera_random(n);
    var n4 = gera_random(n);
    var n5 = gera_random(n);
    var n6 = gera_random(n);
    var n7 = gera_random(n);
    var n8 = gera_random(n);
    var n9 = 0;//gera_random(n);
    var n10 = 0;//gera_random(n);
    var n11 = 0;//gera_random(n);
    var n12 = 1;//gera_random(n);
    var d1 = n12*2+n11*3+n10*4+n9*5+n8*6+n7*7+n6*8+n5*9+n4*2+n3*3+n2*4+n1*5;
    d1 = 11 - ( mod(d1,11) );
    if (d1>=10) d1 = 0;
    var d2 = d1*2+n12*3+n11*4+n10*5+n9*6+n8*7+n7*8+n6*9+n5*2+n4*3+n3*4+n2*5+n1*6;
    d2 = 11 - ( mod(d2,11) );
    if (d2>=10) d2 = 0;
    
    if(comPontos) resultado = ''+n1+n2+'.'+n3+n4+n5+'.'+n6+n7+n8+'/'+n9+n10+n11+n12+'-'+d1+d2;
    else resultado = ''+n1+n2+n3+n4+n5+n6+n7+n8+n9+n10+n11+n12+d1+d2;
    
    return resultado;
}

var Base64 = {
    // private property
    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode : function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
            Base64._keyStr.charAt(enc1) + Base64._keyStr.charAt(enc2) +
            Base64._keyStr.charAt(enc3) + Base64._keyStr.charAt(enc4);
        }

        return output;
    },

    // public method for decoding
    decode : function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = Base64._keyStr.indexOf(input.charAt(i++));
            enc2 = Base64._keyStr.indexOf(input.charAt(i++));
            enc3 = Base64._keyStr.indexOf(input.charAt(i++));
            enc4 = Base64._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }

        output = Base64._utf8_decode(output);

        return output;
    },

    // private method for UTF-8 encoding
    _utf8_encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}

function aleatorio() {
    var aleat = Math.floor(Math.random() * 999);
    if (aleat < 100) {
        if (aleat < 10) {
            return "00"+aleat;
        } else {
            return "0"+aleat;
        }
    } else {
        return aleat;
    }
}

function digPri(n1,n2,n3) {
    var nn1 = n1.split("");
    var nn2 = n2.split("");
    var nn3 = n3.split("");
    var nums = nn1.concat(nn2,nn3);
    
    var x = 0;
    var j = 0;
    for (var i=10;i>=2;i--) {
        x += parseInt(nums[j++]) * i;
    }
    var y = x % 11;
    if (y < 2) {
        return 0;
    } else {
        return 11-y;
    }
}

function digSeg(n1,n2,n3,n4) {
    var nn1 = n1.split("");
    var nn2 = n2.split("");
    var nn3 = n3.split("");
    var nums = nn1.concat(nn2,nn3);
    nums[9] = n4;
    var x = 0;
    var j = 0;
    for (var i=11;i>=2;i--) {
        x += parseInt(nums[j++]) * i;
    }
    var y = x % 11;
    if (y < 2) {
        return 0;
    } else {
        return 11-y;
    }
}

function gera_random(n){
    var ranNum = Math.round(Math.random()*n);
    return ranNum;
}

function mod(dividendo,divisor){
    return Math.round(dividendo - (Math.floor(dividendo/divisor)*divisor));
}
