const secrets = JSON.parse(open(`./secrets.json`));

export const output = {
    'baseUrl': 'https://api-dev.apsen.digital/sap-middleware'
};