import http from 'k6/http';
import { check, sleep } from 'k6';
import { output } from '../../webpack.config.js';

let jsonReturn = "", payload = "";
export function auth() {
    payload = {
        grant_type: 'client_credentials',
        client_id: output['client_id'],
        scope: output['scope'],
        client_secret: output['client_secret']
    };
    
    const res = http.post('https://login.microsoftonline.com/' + output['tenant_id'] + '/oauth2/v2.0/token', payload);

    if (res.status > 399) 
    {
        console.log(res.status + ':' + res.body);
    }  
    else
    {
        console.log(res.status + ':' + res.body);
        check(res, { 'auth status was 200': (r) => r.status == res.status });
    }
    sleep(1);

    jsonReturn = JSON.parse(res.body);

    return jsonReturn['access_token'];
  };