import http from 'k6/http';
import { check, sleep } from 'k6';
import { Trend, Rate, Counter } from "k6/metrics";
import { output } from '../../webpack.config.js';
import { generatorNumberControlClient, gerarCpf } from '../../General/BaseTest.js';
import { auth } from '../Auth/auth.js';

export let createVendorbyCPFDuration = new Trend('create_vendor_by_cpf_duration');
export let createVendorbyCPFFailRate =  new Rate('create_vendor_by_cpf_fail_rate');
export let createVendorbyCPFSuccessRate =  new Rate('create_vendor_by_cpf_success_rate');
export let createVendorbyCPFReqs = new Rate('create_vendor_by_cpf_reqs');

let header = "", payload = "";
export function createVendorbyCPF() {
    payload = JSON.stringify({
        NumeroControleCliente: generatorNumberControlClient(2,2),
        Nome: "ALLAN NUNES",
        Documento: gerarCpf().toString(),
        TermoPesquisa: "ALLAN",
        ContagemReconciliacaoContabilidadeGeral: "211105",
        GrupoAdministracaoTesouraria: "YB05",
        GrupoParaEsquemaCalculo: "YA",
        GrupoContasFornecedor: "YB13",
        MoedaPedido: "BRL",
        InscricaoEstadual: "",
        InscricaoMunicipal: "",
        CodigoIdioma: "PT",
        Contatos: {
            Telefone: "(61) 36235-718",
            Fax: "(61) 3346-0244",
            Celular: "(61) 9267-2979",
            Email: "ALLAN.N.RODRIGUES@HOTMAIL.COM"
        },
        Endereco: {
            Rua: "RUA 233",
            Numero: "19",
            Complemento: "QD 149",
            Bairro: "PARQUE 9",
            Cep: "72853-149",
            Cidade: "LUZIANIA",
            Estado: "GO",
            Pais: "BR",
            Observacao: "NOTA DE OBS"
        },
        ContasBancarias: [
            {
                CodigoPaisBanco: "BR",
                CodigoBanco: "3417",
                Agencia: "4299",
                AgenciaDigito: "1",
                Conta: "70190",
                ContaDigito: "2"
            }
        ],
        Impostos: [
            {
                CodigoCategoriaImpostoRetidoFonte: "IS",
                CodigoImpostoRetidoFonte: "R6",
                CodigoSujeitoIrs: "X"
            },
            {
                CodigoCategoriaImpostoRetidoFonte: "IR",
                CodigoImpostoRetidoFonte: "R2",
                CodigoSujeitoIrs: "X"
            }
        ]
    });

    header = {
        headers: {
          'Authorization': 'Bearer ' + auth(),
          'Content-Type': 'application/json',
          'x-correlation-id': output['x-correlation-id'],
          'Ocp-Apim-Subscription-Key': output['ocp-Apim-Subscription-Key']
        }
      };
    
    const res = http.post(output.baseUrl + '/api/vendor', payload, header);
    createVendorbyCPFDuration.add(res.timings.duration);
    createVendorbyCPFReqs.add(1);
    createVendorbyCPFFailRate.add(res.status == 0 || res.status > 399);
    createVendorbyCPFSuccessRate.add(res.status < 399);

    if (res.status > 399) 
    {
        console.log(res.status + ':' + res.body);
    }  
    else
    {
        console.log(res.status + ':' + res.body);
        check(res, { 'createVendorbyCPF status was 201': (r) => r.status == res.status });
    }
    sleep(1);
  };